# ml-interview-task

The Machine Learning task for an offline interview.

The task definition
===
The task is to train a 3 way sentiment classifier using a deep learning framework of your choice: PyTorch, TensorFlow, Keras (or some other to your liking).
The classifier will operate on sentence level.

Data
===
Please download the annotated dataset for the 3 way sentiment classification: http://help.sentiment140.com/for-students/
Read about the data format on the link as well.

The data package will contain both training set and test test.

For the training file (`training.1600000.processed.noemoticon.csv`), randomly sample 25,000 examples for the training split, and 8,000 examples for the validation split. 
Use the test set (`testdata.manual.2009.06.14.csv`) as it is.

Update: the training file only has positive and negative examples while the test set has positive, negative and neutral examples. So, as a workaround, treat this as a binary classification problem and drop the neutral examples from the test set.

Task
===
You task is to load the training data for encoding and training of a deep learning network. You can choose any architecture of the network you think is best suited
for the sentiment analysis task.

You should implement the following methods and classes:

* `data_to_tensors(input='train.csv')` to convert your data into tensors suitable for encoding
* `Encoder` class's `forward_pass()` method for encoding tensors into vectors, `loss()` function to compute the loss for each sentence as you train.
* `main()` method will orchestrate the entire process: data loading, encoding, and training in several epochs.
* `cal_accuracy(input)` method should compute the accuracy and print it out at the end of each epoch and after the training is complete.
* `inference(string)` should use the trained model and output a polarity label for the given string.

Questions (Explanation Only)
===
1. Let's say that the goal was to use hand-crafted features, semantic or syntatic rules to solve this problem. Based on your intuition or experience, what kind of features would be helpful for this task?
2. If you were to improve the baseline model's performance, what all strategies will you try? Answer as many as possible.
3. Deep Learning models are often termed as 'black-box' models for their lack of interpretability. If the goal was to have an interpretable sentiment model or to somehow explain the predictions of the model, how would you proceed?

Once you are done, submit the following:

* The completed ml-interview-task.py file
* A txt/log file containing training procedure details, progression, and numbers: training/dev/test accuracy and loss
* Answers to the Questions (explanation only)

