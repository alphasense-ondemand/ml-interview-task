# Copy documentation from readme


def data_to_tensors(fname: str ='train.csv'):
    """
    Functions you might need to implement here:
    1. Read data from the file
    2. Tokenize, index and pad (optional)

    3. Return train_tensors: pytorch or tensorflow tensor (could return a batch as well)

    Example: ['i will take care of it'] -> [42, 51, 77, 9, 4, 445]
    
    Indexing: assigning a unique id to each token in the data set. You might want to use a dictionary for that.
    """

    pass


class Encoder(object):
    def __init__(self):
        pass

    def forward_pass(self, input):
        """
        Return the encoded representation of the input sentence/batch
        If a sentence, the returned encoded representation should be a vector of size (1, H)
        Otherwise, for a batch, the shape should be (B, H) where B=batch_size, and H=hidden dim are hyperparameters
        """

        pass

    def loss_fn(self, input, label):
        """You can keep as part of the class or you can implement it separately as well"""

        pass


def cal_accuracy(input):
    """
    This function should calculate accuracy (or any other metric) for a given file (train/valid/test)
    You will be calling this function during training and once after training finishes on all three data splits

    The input can be a file name (read the data; cal accuracy) or if you have read the data splits and processed them,
    you can pass that as well. The output should be a float (either between 0 and 1, or between 0 and 100%)
    """

    pass


def inference(example: str):
    """
    This is a test function to get the prediction for a given string. The output should be an integer (0/1/2) or
    a string ('negative', 'neutral', 'positive'), and a confidence score (softmax probability)
    """

    pass


def main():
    """
    Write your implementation here:
      The pipeline would look something like:
      1. Load the data
      2. Preprocess it and convert it into tensor(s)
      3. Decide on the encoder model, the  optimizer, and the loss function based on the problem at hand
      3. For m epochs: read the data, encode it, calculate the loss, the gradients, and optimize the model parameters
      4. Also, print out some scores (loss value, accuracy) to show that the model is learning and the performance
         is improving
      5. After training is finished, use the trained model to make predictions on some random examples
    """
